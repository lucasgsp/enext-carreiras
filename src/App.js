import './App.css';
import CardsHolder from './components/CardsHolder/CardsHolder';
import CenterImageHolder from './components/CenterImageHolder/CenterImageHolder';
import DiversityContent from './components/DiversityContent/DiversityContent';
import FooterHolder from './components/FooterHolder/TextHolder';
import Header from './components/Header/Header';
import ImageHolder from './components/ImageHolder/ImageHolder';
import JobsContent from './components/JobsContent/JobsContent';
import SimpleSlider from './components/PicturesCarousel/PicturesCarousel';
import PodcastHolder from './components/PodcastHolder/PodcastHolder';
import TextHolder from './components/TextHolder/TextHolder';
import VideoHolder from './components/VideoHolder/VideoHolder';

function App() {
  return (
    <div className="App">
      <Header></Header>
      <ImageHolder></ImageHolder>
      <TextHolder></TextHolder>
      <CardsHolder></CardsHolder>
      <VideoHolder></VideoHolder>
      <SimpleSlider></SimpleSlider>
      <CenterImageHolder></CenterImageHolder>
      <JobsContent></JobsContent>
      <DiversityContent></DiversityContent>
      <PodcastHolder></PodcastHolder>
      <FooterHolder></FooterHolder>
    </div>
  );
}

export default App;
