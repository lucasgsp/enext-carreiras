import React from 'react'
import { Holder } from './styled'

function VideoHolder() {
    return (
            <Holder>
                <video controls>
                    <source src='Campanha.mp4' />
                </video>
            </Holder>

    )
}

export default VideoHolder
