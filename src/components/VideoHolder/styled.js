import styled from "styled-components";

export const Holder = styled.div`
    display: flex;
    justify-content: center;
    filter: drop-shadow(-50px 50px 10px rgba(0, 0, 0, 1));
    margin: 0 auto;
    @media (min-width:100px) and (max-width:1000px){
        width: 400px !important;
        height: 200px !important;
        /* align-items: baseline !important; */
        filter: drop-shadow(-10px 10px 5px rgba(0, 0, 0, 1)) !important;
        margin-top: 50px !important;
        margin-left: 0 !important;
    }
`