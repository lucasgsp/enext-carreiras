import React from 'react'
import { Title, Page, Info } from './styled'

function TextHolder() {
    return (
        <Page>
            <Title>
                <span>+</span>
                <h1>TRANSFORMANDO AS PESSOAS PARA O FUTURO</h1>
            </Title>
            <Info>
                <h2><b>Nossa cultura gira em torno do nosso maior objetivo:</b> transformar as pessoas para o futuro.</h2>
                <p>Criamos espaços onde o crescimento pessoal e profissional de cada um se transforma em meta coletiva. Você aprende e se desenvolve diariamente com apoio da liderança e do time de People da Enext</p>
                <p>Lideramos pessoas hoje, para que se tornem líderes inspiradores no futuro</p>
            </Info>
        </Page>
    )
}

export default TextHolder
