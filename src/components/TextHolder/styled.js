import styled from "styled-components";


export const Title = styled.div`
    display: flex;
    width: 800px;
    height: 100%;
    margin:50px 50px 50px 80px;
    flex-direction: column;
    word-wrap: break-word;
    span{
        width: 100px;
        height: 20px;
        font-size:3rem;
    }
    h1{
        font-size:4rem;
        width: 800px;
    }
    @media (min-width:100px) and (max-width:1000px){
    display: flex;
    width: 330px;
    height: 100%;
    margin:50px 50px 20px 20px !important;
    flex-direction: column;
    span{
        font-weight: 500;
    }
    h1{
        margin-top: 40px;
        width: 330px;
        font-size: 2rem;
    }
    }
`
export const Page = styled.div`
    display: flex;
    height: 100%;
    @media (min-width:100px) and (max-width:1000px){
    flex-direction: column;
    }
`
export const Info = styled.div`
    margin:120px 50px 50px 50px;
    width: 900px;
    height: 100%;
    word-wrap: break-word;
    h2{
        font-weight: 400;
        font-size: 1.3rem;
        margin:0 0 5px 0;
    }
    p{
        font-weight: 400;
        font-size: 1.3rem;
        margin:0 0 8px 0;
    }
    @media (min-width:100px) and (max-width:1000px){
    margin:0 50px 50px 20px !important;
    width: 900px;
    height: 100%;
    word-wrap: break-word;
    h2{
        width: 335px;
        font-weight: 400;
        font-size: 1.1rem !important;
        margin:0 0 5px 0;
    }
    p{
        width: 335px;
        font-weight: 400;
        font-size: 1.1rem !important;
        margin:0 0 8px 0;
    }
    }
`