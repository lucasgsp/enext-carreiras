import React from 'react'
import { CardContainer, Title, Page } from './styled'

function FooterHolder() {
    return (
        <Page>
            <Title>
                <span>+</span>
                <h1>#TRANSFORMANDO AS PESSOAS PARA O FUTURO</h1>
            </Title>
            <CardContainer>
                <div>
                    <a href='https://www.linkedin.com/company/enext-ecommerce' target="_blank" rel="noreferrer" ><img src='linkedin.svg' alt=''/></a>
                </div>
                <div>
                    <a href='https://pt-br.facebook.com/enextconsultoria/' target="_blank" rel="noreferrer"><img src='fb.svg' alt=''/></a>
                </div>
                <div>
                    <a href='https://www.instagram.com/enext_/' target="_blank" rel="noreferrer"><img src='insta.svg' alt=''/></a>
                </div>
                <div>
                    <a href='https://www.glassdoor.com.br/Vis%C3%A3o-geral/Trabalhar-na-Enext-EI_IE2379752.13,18.htm' target="_blank" rel="noreferrer"><img src='glass.svg' alt=''/></a>
                </div>
            </CardContainer>
        </Page>
    )
}

export default FooterHolder
