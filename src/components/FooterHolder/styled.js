import styled from "styled-components";


export const Title = styled.div`
    display: flex;
    width: 800px;
    height: 100%;
    background-color: black;
    margin:50px 50px 50px 80px;
    flex-direction: column;
    span{
        width: 100px;
        height: 20px;
        font-size:3rem;
        color:white;
    }
    h1{
        font-size:4rem;
        width: 800px;
        color: white;
    }
    @media (min-width:100px) and (max-width:1000px){
    width: 300px !important;
    margin-left: 30px !important;
    margin-bottom: 15px !important;
    margin-top: 20px !important;
    span{

    }
    h1{
        margin-top: 35px;
        font-size: 2rem !important;
        width: 300px !important;
    }
    }
`

export const CardContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    img{
        margin-left: 20px;
    }
    @media (min-width:100px) and (max-width:1000px){
    display: grid;
    grid-template-columns: 160px 160px;
    grid-template-rows: 200px 200px;
    align-items: center;
    justify-content: center;
    margin-bottom: 20px !important;
    img{
        margin-left:13px !important;
    }
    }
`
export const Page = styled.div`
    display: flex;
    height: 100%;
    background-color: black;
    align-items: center;
    justify-content: center;
    @media (min-width:100px) and (max-width:1000px){
    flex-direction: column;
    }
`
