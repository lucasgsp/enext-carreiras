import React from 'react'
import { Content, TitleHolder, TitleHolderRight, ContentRight, InfoContentRight, InfoContent, PageHolder } from './styled'


function CenterImageHolder() {
    return (
        <PageHolder>
            <Content>
                <TitleHolder>
                    <span>+</span>
                    <h1>TRANSFORMADOS PARA O FUTURO</h1>
                </TitleHolder>
                <InfoContent>
                    <p>
                        Faz parte da nossa missão desenvolver e capacitar talentos para o mundo. Incentivamos nosso time a desafiar as regras e as convenções estabelecidas. Solucionamos problemas complexos com criatividade e autonomia. Acreditamos no potencial individual e na construção coletiva. Quer saber quem são os Enexters que estão revolucionando o mercado digital? Conheça o nosso Golden Team.
                    </p>
                </InfoContent>
            </Content>
            <img src='woman.svg' alt='woman' />
            <ContentRight>
                <TitleHolderRight>
                    <h1>Vitória Braga</h1>
                    <h2>HEAD OF CRO & BI</h2>
                </TitleHolderRight>
                <InfoContentRight>
                    <p>
                    Minha trajetória na Enext teve início em 2018, 
como estagiária da área de CRO, que na época 
ainda estava sendo estruturada. Após três 
anos de dedicação e contato diário com todas 
as áreas da empresa, pude desenvolver 
diversos projetos muito especiais e cases. 
Atualmente sou a Coordenadora e responsável 
pelo time de CRO e de BI (Business 
Intelligence) da Enext. A Enext foi o lugar que me abriu as portas para 
o mercado digital. Aqui tive a oportunidade de 
aprender e muita liberdade para inovar. Depois 
de todos esses anos, sinto que fiz a escolha 
certa para o meu futuro.
                    </p>
                </InfoContentRight>
            </ContentRight>
        </PageHolder>
    )
}

export default CenterImageHolder
