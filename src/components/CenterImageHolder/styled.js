import styled from "styled-components";

export const Content = styled.div`
    width: 550px;
    height: 100%;
    display: flex;
    flex-direction: column;
    margin-left: 40px;
`
export const TitleHolder = styled.div`
    display: flex;
    width: 520px;
    background-color: black;
    margin:50px 50px 50px 0;
    flex-direction: column;
    word-wrap: break-word;
    span{
        width: 100px;
        height: 20px;
        font-size:3rem;
        color:white;
    }
    h1{
        font-size:3.5rem;
        width: 500px;
        color:white;
    }
    h2{
        color:white;
    }
    @media (min-width:100px) and (max-width:1000px){
        margin-bottom: 10px !important;
        margin-top: 20px !important;
        h1{
            margin-top: 40px !important;
            font-size: 2rem !important;
            width: 300px !important;
        }
    }
`
export const TitleHolderRight = styled.div`
    display: flex;
    width: 520px;
    background-color: black;
    margin:50px 50px 50px 0;
    flex-direction: column;
    word-wrap: break-word;
    span{
        width: 100px;
        height: 20px;
        font-size:3rem;
        color:white;
    }
    h1{
        font-size:3.5rem;
        width: 500px;
        color:white;
        margin: 50px 0 0 0;
    }
    h2{
        color:white;
        margin: 0 0 0 0;
        font-weight: 500;
    }
    @media (min-width:100px) and (max-width:1000px){
        margin-top: 40px !important;
        h1{
            margin: 20px 0 0 20px !important;
            font-size:2.5rem !important;
            font-weight: 600 !important;
        }
        h2{
            margin-left:23px !important;
            font-size: 1.1rem !important;
        }
    }
`

export const PageHolder = styled.div`
    display: flex;
    width: 100vw;
    height: 100%;
    background-color: black;
    justify-content: center;
    img{
        margin: -50px 50px 50px 50px;
        width: 500px;
        filter: drop-shadow(1px 100px 100px #FFB324);
    }
    @media (min-width:100px) and (max-width:1000px){
        flex-direction: column !important;
        img{
            width: 300px !important;
            margin: 0 auto !important;
            filter: drop-shadow(1px 10px 50px #FFB324) !important;
        }
    }
`
export const InfoContentRight = styled.div`
    margin:0 !important;
    width: 100%;
    height: 100%;
    word-wrap: break-word;
    h2{
        font-weight: 400;
        font-size: 1.5rem;
        margin:0 0 5px 0;
    }
    p{  
        margin:0 0 15px 0 !important;
        font-weight: 400;
        font-size: 1.5rem;
        width: 500px;
        color: white;
    }
    @media (min-width:100px) and (max-width:1000px){
        p{
            width: 335px !important;
            font-size: 1rem;
            margin: 0 0 30px 20px !important;
        }

    }
`
export const InfoContent = styled.div`
    margin:0 !important;
    width: 100%;
    height: 100%;
    word-wrap: break-word;
    h2{
        font-weight: 400;
        font-size: 1.5rem;
        margin:0 0 5px 0;
    }
    p{  
        margin:0 0 15px 0 !important;
        font-weight: 400;
        font-size: 1.5rem;
        width: 500px;
        color: white;
    }
    @media (min-width:100px) and (max-width:1000px){
        p{
            width: 300px !important;
            font-size: 1rem !important;
        }
    }
`
export const ContentRight = styled.div`
    width: 530px;
    height: 100%;
    display: flex;
    flex-direction: column;
    margin-right: 40px;
`