import styled from "styled-components";

export const Card = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: white;
    margin: 40px 15px 60px 15px;
    width: 280px;
    height: 400px;
    filter: drop-shadow(-1px 1px 10px rgba(0, 0, 0, 0.3));
    h1{
        margin:40px 40px 0 40px;
        text-align:justify;
        width: 245px;
        color:black;
        text-align:center;
        font-size: 1.8rem;
    }
    p{  
        margin-top:30px;
        text-align:center;
        width: 220px;
        position: absolute;
        top:90px;
    }
    /* <--desktop--(1300px <)--> */
    @media (min-width:1153px) and (max-width:1495px){
    display: flex;
    flex-direction: column;
    /* align-items: center; */
    background-color: white;
    margin: 60px 15px 60px 15px;
    width: 280px;
    height: 400px;
    filter: drop-shadow(-1px 1px 10px rgba(0, 0, 0, 0.3));
    }
`
export const Page = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    @media (min-width:100px) and (max-width:1000px){
        overflow-x: scroll;
        justify-content: normal !important;
        align-items: default !important;
    }
`