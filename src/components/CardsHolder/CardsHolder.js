import React from 'react'
import { Card, Page } from './styled'

function CardsHolder() {
    return (
        <Page>
            <Card>
                <h1>PROGRAMAS DE CAPACITAÇÃO</h1>
                <p>Feito para desenvolver jovens talentos que estão na faculdade, o programa de capacitação é uma grande oportunidade para quem se interessa em entrar no mercado de tecnologia e marketing. Com essa iniciativa a Enext fomra, por ano, cerca de 100 novos talentos e metade deles conseugme uma vaga de estágio após este período.</p>
            </Card>
            <Card>
                <h1>CERTIFICAÇÕES</h1>
                <p>Nosso compromisso é a sua evolução, por isso os Enexters têm acesso ilimitado a treinamentos relacionados às principais plataformas de
                    e-commerce do mercado, sendo elas VTEX, Oracle e Salesforce.</p>
            </Card>
            <Card>
                <h1>TRILHA DE CARREIRAS</h1>
                <p>Aqui você se desenvolve de verdade! Com a ajuda da liderança imediata e time de desenvolvimento, você constrói, trilha sua carreira,e se torna um profissional diferenciado no mercado de tecnologia.</p>
            </Card>
            <Card>
                <h1>DIVERSIDADE</h1>
                <p>Buscamos a evolução em todos os nossos processos e é por isso que o time de Talent Acquisition coloca em prática um processo livre de vieses inconscientes, para que os próximos Enexters possam ser quem eles exatamente são aqui dentro. Além disso, contamos também com o Comitê de Diversidade liderado por nossos colaboradores.</p>
            </Card>
            <Card>
                <h1>TREINAMENTO DE LIDERANÇA</h1>
                <p>A busca pela evolução é constante aqui na Enext e não pode ser diferente com os nossos líderes. O Programa de liderança visa o desenvolvimento técnico e humano das nossas referências para gerar uma experiência ainda mais incrível a todos os nossos colaboradores.</p>
            </Card>
        </Page>
    )
}

export default CardsHolder
