import React from 'react'
import { Title, PageHolder, Cards, CardHeader, Jobs, BlackCard } from './styled'

function JobsContent() {
    return (
        <PageHolder>
            <Title>
                <span>+</span>
                <h1>NOSSAS VAGAS</h1>
            </Title>
            <Jobs>
                <Cards>
                    <CardHeader>
                        <span>+</span>
                    </CardHeader>
                    <h1>Programa de Capacitação VTEX</h1>
                    <h2>BRASIL | São Paulo</h2>
                    <p>A Enext é uma consultoria e agência digital, formada por um time super engajado que tem hoje aproximadamente 200 pessoas.</p>
                    <a href='https://enext.solides.jobs/vacancies/89906#vacancyDescription' target="_blank" rel="noreferrer">SAIBA MAIS</a>
                </Cards>
                <Cards>
                    <CardHeader>
                        <span>+</span>
                    </CardHeader>
                    <h1>ESTÁGIO EM CRM TECH</h1>
                    <h2>BRASIL | São Paulo</h2>
                    <p>A Enext é uma consultoria e agência digital, formada por um time super engajado que tem hoje aproximadamente 200 pessoas.</p>
                    <a href='https://enext.solides.jobs/vacancies/86917#vacancyDescription' target="_blank" rel="noreferrer">SAIBA MAIS</a>
                </Cards>
                <Cards>
                    <CardHeader>
                        <span>+</span>
                    </CardHeader>
                    <h1>BANCO DE TALENTOS</h1>
                    <h2>BRASIL | São Paulo</h2>
                    <p>A Enext é uma consultoria e agência digital, formada por um time super engajado que tem hoje aproximadamente 200 pessoas.</p>
                    <a href='https://enext.solides.jobs/talentBankApply' target="_blank" rel="noreferrer">SAIBA MAIS</a>
                </Cards>
                <BlackCard>
                    <CardHeader>
                        <span>+</span>
                    </CardHeader>
                    <h1>E TEM MUITO MAIS! CONFIRA TODAS AS VAGAS ABERTAS E #VEMPRAENEXT :)</h1>
                    <a href='https://enext.solides.jobs/' target="_blank" rel="noreferrer">SAIBA MAIS</a>
                </BlackCard>
            </Jobs>
        </PageHolder>
    )
}

export default JobsContent
