import styled from "styled-components";


export const Title = styled.div`
    display: flex;
    width: 100%;
    height: 100%;
    /* background-color: green; */
    flex-direction: column;
    span{
        width: 100px;
        height: 20px;
        font-size:3rem;
        margin-left: 80px;
    }
    h1{
        font-size:4rem;
        width: 800px;
        margin-left:80px;
    }
    @media (min-width:100px) and (max-width:1000px){
        margin-top: -30px !important;
        span{
            margin-left: 17px !important;
        }
        h1{
            font-size: 2rem !important;
            margin-left: 20px !important;
            margin-top:35px !important;
        }
    }
`

export const PageHolder = styled.div`
    margin-top: 50px;
    display: flex;
    align-items: center;
    width:100%;
    flex-direction: column;
    /* background-color: purple; */
`

export const Cards = styled.div`
    display: flex;
    width: 400px;
    height: 325px;
    margin:0 0 20px 20px;
    flex-direction: column;
    background-color: white;
    filter: drop-shadow(-1px 1px 10px rgba(0, 0, 0, 0.3));
    h1{
        margin-left: 30px;
        height: 64px;
        font-size: 1.7rem;
        font-weight: 500;
    }
    h2{
        margin: 0 0 0 30px;
        font-weight: 300;
    }
    p{
        margin: 30px 0 30px 30px;
        font-weight: 500;
        width: 350px;
    }
    a{
        margin:0 auto;
        width: 350px;
        height: 40px;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: black;
        color:white;
        text-decoration:none;
        font-weight: 900;
        font-size: 1.2rem;
    }
`
export const CardHeader = styled.div`
    display: flex;
    flex-direction: row-reverse;
    align-items: center;
    width: 100%;
    height: 20px;
    background-color: black;
    span{
        color:white;
        margin-right: 10px;
        font-weight: bold;
        font-size:1.2rem;
    }
`

export const Jobs = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    @media (min-width:100px) and (max-width:1000px){
        width: 400px !important;
        overflow-x: scroll !important;
        justify-content: normal !important;
    }
`

export const BlackCard = styled.div`
    display: flex;
    width: 400px;
    height: 325px;
    margin:0 0 20px 20px;
    flex-direction: column;
    background-color: black;
    h1{
        text-align:center;
        margin: 50px 20px 60px 20px;
        font-size: 1.8rem;
        font-weight: 900;
        color: white;
    }
    a{
        margin:0 auto;
        width: 350px;
        height: 40px;
        display: flex;
        align-items: center;
        justify-content: center;
        background-color: white;
        color:black;
        text-decoration:none;
        font-weight: 900;
        font-size: 1.2rem;
    }
    @media (min-width:100px) and (max-width:1000px){
    margin-right: 20px !important;
    }
`
