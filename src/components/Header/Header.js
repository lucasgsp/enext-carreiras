import React, { useState } from 'react'
import { Logo, LogoWithLinks } from './styled';

function Header() {
    const [active, setActive] = useState(0)
    console.log(active)
    function MobileNavbar() {
        if (active === 0) {
            setActive(1)
            var menu = document.querySelector('ul')
            menu.setAttribute('style', 'transform:translateX(0%)')

        } else {
            setActive(0)
            var menu_ = document.querySelector('ul')
            menu_.setAttribute('style', 'transform:translateX(100%)')
        }
    }
    return (
        <div>
            <LogoWithLinks>
                <Logo>
                    <img src='logo.svg' alt='logo' />
                </Logo>
                <div>
                    <img onClick={() => MobileNavbar()} src='menuicon.png' alt='menu' />
                </div>
                <div>
                    <ul>
                        <li><a href>SOBRE NÓS</a></li>
                        <li><a href>CERTIFICAÇÕES</a></li>
                        <li><a href>CLIENTES</a></li>
                        <li><a href>LIDERANÇA</a></li>
                        <li><a href>LABS</a></li>
                        <li><a href>CARREIRAS</a></li>
                        <li><a href>CONTATO</a></li>
                        <li><a href>IMPRENSA</a></li>
                    </ul>
                </div>
            </LogoWithLinks>
        </div>
    )
}

export default Header
