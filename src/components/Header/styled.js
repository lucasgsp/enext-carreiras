import styled from "styled-components";

export const LogoWithLinks = styled.div`
    display: flex;
    width: 100%;
    height: 80px;
    position: fixed;
    z-index: 3;
    top: 0;
    flex-direction: row;
    background-color: black;
    align-items: center;
    justify-content: center;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    a{
        color:white;
        text-decoration: none;
        margin: 0 1vw 0 1vw;
        font-size: 1rem;
        font-weight:400;
    }
    a:hover{
        font-weight: 500;
    }
    div{
        background-color: black;
        height: 60px;
        display: flex;
        align-items: center;
    }
    ul{
        display: flex;
        list-style: none;
    }
    img{
        display: none;
        cursor: pointer;
    }

    @media (min-width:200px) and (max-width:1174px){
        body{
            overflow-x:hidden;
        }
        ul{
            padding: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-around;
            margin: 0;
            position: absolute;
            top:9vh;
            right: 0;
            width: 50vw;
            height:92vh;
            background-color: black;
            transform: translateX(100%);
            transition: transform 0.3s ease-in;
            opacity: 0.9 ;
        }
        li{
            display: flex;
        }
        img{
            display: block;
            position: absolute;
            right:10vw;
            width: 32px;
        }
        a{
            text-align: center;
        }
    }
`
export const Logo = styled.div`
    display: flex;
    position: absolute;
    left: 10px;
    img{
        display: flex;
        width: 80px !important;
        margin-left:40px;
        cursor: default;
    }
    @media (min-width:200px) and (max-width:1174px){
        img{
            width: 80px !important;
            position:absolute !important;
            left:0.1vw !important;
            margin-left: 14px !important;
        }
    }
`