import styled from "styled-components";

export const PageHolder = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: -110px;
    position: relative;
    /* align-items: center; */
    justify-content: center;
    @media (min-width:100px) and (max-width:1000px){
        flex-direction: column !important;
    }
`
export const TextContent = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
`
export const TitleHolder = styled.div`
    display: flex;
    flex-direction: column;
    span{
        width: 100px;
        height: 20px;
        font-size:3rem;
        margin-left: 80px;
    }
    h1{
        font-size:4rem;
        width: 400px;
        margin-left:80px;
    }
    @media (min-width:100px) and (max-width:1000px){
        margin-top: -80px !important;
    span{
        margin-left: 18px !important;
    }
    h1{
        margin-left: 22px !important;
        margin-top: 30px !important;
        width: 400px !important;
        font-size: 1.8rem !important;
    }
    }
`
export const TextHolder = styled.div`
    display: flex;
    p{
        width: 600px;
        font-size: 1.5rem;
        font-weight: 300;
        margin: 0 0 0 80px;
    }
    @media (min-width:100px) and (max-width:1000px){
    p{
        margin-left: 20px !important;
        font-size: 1.2rem !important;
        font-weight: 300 !important;
    }
    }
`
export const BigX = styled.div`
    display: flex;
    width: 550px;
    height: 500px;
    background-image: url('bigx.svg') ;
    background-position: -220px;
    background-repeat: no-repeat;
    align-items: center;
    justify-content: center;
    @media (min-width:100px) and (max-width:1000px){
    margin-top: -42px !important;
    background-size: 600px !important;
    width: 380px;
    }
`
export const SpotifyContent = styled.div`
    display: flex;
    flex-direction: column;
    background-image: url('circle.svg');
    background-repeat: no-repeat;
    background-position: center;
    width: 500px;
    align-items: center;
    justify-content: center;
    img{
        width: 300px;
    }
    h1{
        margin: -50px 0 30px 0;
        color: white;
    }
    @media (min-width:100px) and (max-width:1000px){
    height: 500px !important;
    background-size: 350px !important;
    background-position: 20px !important;
    img{
        width: 250px !important;
        margin-left:-100px !important;
    }
    h1{
        margin-left:-100px !important;
        font-size: 1.4rem !important;
    }
    }
`
