import React from 'react'
import { PageHolder, TextContent, BigX, SpotifyContent, TitleHolder, TextHolder } from './styled'
function PodcastHolder() {
    return (
        <PageHolder>
            <BigX>
                <img src='xcast.svg' alt='logo'/>
            </BigX>
            <TextContent>
                <TitleHolder>
                    <span>+</span>
                    <h1>O PODCAST DA ENEXT</h1>
                </TitleHolder>
                <TextHolder>
                    <p>Vem ouvir mais sobre tecnologia e ecommerce com os especialistas da Enext e convidados. Disponível nas principais plataformas digitais.</p>
                </TextHolder>
            </TextContent>
            <SpotifyContent>
                <h1>ACOMPANHE:</h1>
                <a href='https://open.spotify.com/show/5ZlY4DptDl1TpWS1Ri9oYF' target='_blank' rel="noreferrer" ><img src='spotify.svg' alt=''/></a>
            </SpotifyContent>
        </PageHolder>
    )
}

export default PodcastHolder
