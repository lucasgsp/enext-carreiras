import React from 'react'
import { PageHolder, TitleHolder, TextHolder, CarouselHolder, GeneralTexts, Content, ImageOne, ImageTwo } from './styled'
import DiversitySlider from './CarouselImages'
function DiversityContent() {
    return (
        <div>
            <PageHolder>
                <Content>
                    <GeneralTexts>
                        <TitleHolder>
                            <span>+</span>
                            <h1>DIVERSIDADE ENEXT</h1>
                        </TitleHolder>
                        <TextHolder>
                            <p>Acreditamos no potencial da diversidade e somos uma empresa que busca cada vez mais ser referência neste assunto. Estimulamos a igualdade através de diálogos e debates com o objetivo de causar impacto efetivo na sociedade.
                                Temos um Comitê de Diversidade que é responsável por trazer maior visibilidade ao assunto e promover eventos como o #PrideTalks. Vem construir uma sociedade mais justa e diversa com a gente! #somosdiversos</p>
                        </TextHolder>
                    </GeneralTexts>
                    <CarouselHolder>
                        <ImageOne>
                            <img src='pride1.svg' alt=''></img>
                        </ImageOne>
                        <DiversitySlider></DiversitySlider>
                        <ImageTwo>
                            <img src='pride2.svg' alt=''></img>
                        </ImageTwo>
                    </CarouselHolder>
                </Content>
            </PageHolder>
        </div>
    )
}

export default DiversityContent
