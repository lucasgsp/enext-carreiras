import React from "react";
import Slider from "react-slick";
import { Holder } from "./styled";

function PrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{ ...style, display: "block", background: "black", borderRadius:"50px" }}
            onClick={onClick}
        />
    );
}

export default function DiversitySlider() {
    var settings = {
        infinite: true,
        centerMode: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: <PrevArrow/>,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        initialSlide: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
    };
    return (
        <Holder>
            <Slider {...settings} >
                <div>
                    <img src='fot1.svg' alt='peoples' />
                </div>
                <div>
                    <img src='fot2.svg' alt='peoples' />
                </div>
                <div>
                    <img src='fot3.svg' alt='peoples' />
                </div>
            </Slider>
        </Holder>
    );
}
