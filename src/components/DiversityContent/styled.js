import styled from "styled-components";

export const PageHolder = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
    height: 100%;
    background-color: #EEE;
    margin-bottom:100px;
    position: relative;
    z-index:1;
`
export const TitleHolder = styled.div`
    display: flex;
    flex-direction: column;
    span{
        width: 100px;
        height: 20px;
        font-size:3rem;
        margin-left: 80px;
    }
    h1{
        font-size:4rem;
        width: 500px;
        margin-left:80px;
    }
    @media (min-width:100px) and (max-width:1000px){
        margin-top: -70px;
        span{
            margin-left: 20px !important;
        }
        h1{
            margin-left:20px !important;
            margin-top: 35px !important;
            width: 400px !important;
            font-size: 2rem !important;
        }
    }
`
export const TextHolder = styled.div`
    display: flex;
    flex-direction: column;
    p{
        width: 500px;
        font-size: 1.5rem;
        font-weight: 400;
        margin-left: 80px;
    }
    @media (min-width:100px) and (max-width:1000px){
        margin-top: -70px;
        p{
            margin-left:20px !important;
            margin-top: 90px !important;
            width: 330px !important;
            font-size:1.2rem !important;
            font-weight: 300 !important;
        }
    }
`
export const GeneralTexts = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 100px;
`


export const CarouselHolder = styled.div`
    display: flex;
    flex-direction: column;
    margin-left:300px;
    margin-top: 100px;
    @media (min-width:100px) and (max-width:1000px){
        margin-left:20px !important;
        margin-top: 10px !important;
    }
`
export const Content = styled.div`
    display: flex;
    justify-content: space-between;
    @media (min-width:100px) and (max-width:1000px){
        flex-direction: column !important;
    }
`


export const Holder = styled.div`
    margin-top: -10px;
    margin-bottom:-10px;
    margin-left: -100px;
    width: 1202px;
    display: default;
    img{
        width: 500px;
        height:500px;
    }
    @media (min-width:100px) and (max-width:1000px){
        margin: 20px -40px 20px -30px !important;
        img{
            width: 300px;
            height: 300px;
        }
    }
`
export const ImageOne = styled.div`
    display: flex;
    img{
        width: 1000px;
        margin-left: -120px;
    }
    @media (min-width:100px) and (max-width:1000px){
        img{
            width: 400px !important;
            margin-left: 50px !important;
            margin-bottom: -20px !important;
        }
    }
`
export const ImageTwo = styled.div`
    display: flex;
    img{
        width: 1200px;
        margin-right:-120px ;
        margin-bottom: 50px;
    }
    @media (min-width:100px) and (max-width:1000px){
        img{
            width: 400px !important;
            margin-left: 160px !important;
            margin-top: -27px !important;
        }
    }
`
