import styled from "styled-components";

export const Holder = styled.div`
    margin-top: 150px;
    margin-bottom:120px;
    /* margin-left:50px; */
    img{
        width: 30vw;
        height:18vw;
    }
    @media (min-width:100px) and (max-width:1000px){
    margin-top: 80px;
    margin-bottom:80px;
    img{
        width: 250px !important;
        height: 175px !important;
    }
    }
`