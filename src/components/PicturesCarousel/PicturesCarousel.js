import React from "react";
import Slider from "react-slick";
import { Holder } from "./styled";


export default function SimpleSlider() {
    var settings = {
        className: "center",
        centerMode: true,
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 3,
        speed: 500,
        dots: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    return (
        <Holder>
            <Slider {...settings} >
                <div>
                    <img src='pic1.jpg' alt='peoples' />
                </div>
                <div>
                    <img src='pic2.jpg' alt='peoples' />
                </div>
                <div>
                    <img src='pic3.png' alt='peoples' height='200px' />
                </div>
                <div>
                    <img src='pic4.jpg' alt='peoples' />
                </div>
            </Slider>
        </Holder>
    );
}
