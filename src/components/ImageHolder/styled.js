import styled from "styled-components";

export const Holder = styled.div`
    margin-top: 4.1vw;
    display: flex;
    width: 100vw;
    height: 750px;
    background-image: url('img1.jpg');
    background-size: 100vw;
    background-position:100% 14%;
    justify-content: left;
    position:relative; 
    z-index:-1;
    h1{
        margin: 50px;
        position: absolute;
        top: 80px;
        color:transparent;
        -webkit-text-stroke-width: 2px;
        -webkit-text-stroke-color: white;
        font-size:18rem;
        width: 710px;
        line-height: 0.8;
        word-wrap: break-word;
        opacity: 0.4;
    }
    h2{
        font-size:1.9rem;
        color: white;
        margin: 50px 50px 50px 60px;
        position: absolute;
        top:201px;
    }
    h3{
        color: white;
        font-size: 12rem;
        margin: 50px 50px 50px 50px;
        position:absolute;
        top:210px;
    }
    @media (min-width:100px) and (max-width:1000px){
    margin-top: 4.1vw;
    display: flex;
    width: 800px;
    height: 570px;
    background-image: url('img1.jpg');
    background-size:160vw 80vh !important;
    background-position:-3vw 50px !important;
    background-repeat: no-repeat !important;
    justify-content: left;
    position:relative; 
    z-index:-1;
    h1{
        margin: 10px !important;
        position: absolute;
        top: 250px;
        color:transparent;
        -webkit-text-stroke-width: 2px;
        -webkit-text-stroke-color: white;
        font-size:9rem !important;
        width: 355px;
        line-height: 0.8;
        word-wrap: break-word;
        opacity: 0.4;
    }
    h2{
        font-size:1.5rem !important;
        color: white;
        margin: 50px 50px 50px 10px;
        position: absolute;
        top:280px;
    }
    h3{
        color: white;
        font-size: 4.5rem;
        margin: 50px 50px 50px 10px;
        position:absolute;
        top:305px;
    }
    }
    /* Animations */
    @-webkit-keyframes fadeInLeft {
        0% {
        opacity: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
}
    100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
}
}
    @keyframes fadeInLeft {
        0% {
        opacity: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
}
    100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
    }
}
    h2{
        -webkit-animation-name: fadeInLeft;
        animation-name: fadeInLeft;
        -webkit-animation-duration: 1s;
        animation-duration: 1.5s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
}
    h3{
        -webkit-animation-name: fadeInLeft;
        animation-name: fadeInLeft;
        -webkit-animation-duration: 1s;
        animation-duration: 2s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
}
    @-webkit-keyframes fadeInLeft {
        0% {
        opacity: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
}
    100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
}
}
    @keyframes fadeInLeft {
        0% {
        opacity: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
}
    100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
    }
} 
`